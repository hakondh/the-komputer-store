import { MOCK_COMPUTERS } from "./mocks/computers.mocks.js";

// Money values
let bankBalance = 0;
let loan = 0;
let hasLoaned = false;
let payBalance = 0;

//Button elements
const btnGetLoan = document.getElementById('get-loan-btn');
const btnBank = document.getElementById('bank-btn');
const btnWork = document.getElementById('work-btn');
const btnRepay = document.getElementById('repay-btn');
const btnBuy = document.getElementById('buy-btn');

// Elements to be modified
const spnBankBalance = document.getElementById('bank-balance');
const spnPayBalance = document.getElementById('pay-balance');
const spnLoanBalance = document.getElementById('loan-balance');
const sctComputer = document.getElementById('computer-select');
const hedComputer = document.getElementById('computer-title');
const pComputerDesc = document.getElementById('computer-desc');
const spnComputerPrice = document.getElementById('computer-price');
const imgComputer = document.getElementById('computer-img');
const ulComputerFeatures = document.getElementById('computer-features');
const pLoanTag = document.getElementById('loan-tag');

// Get loan event listener
btnGetLoan.addEventListener('click', () => {
    // Check if the user is allowed to get a loan
    if(loan !== 0 || hasLoaned) {
        // Create a customized alert message based on the conditions
        const alertMsg =
            (hasLoaned ? 'You need to buy a computer ' + ((loan !== 0) ? 'and ' : '') : '') +
            ((loan !== 0) ? (hasLoaned ? 'y' : 'Y') + 'ou need to pay back your current loan ' : '') +
            'to get another loan.';
        alert(alertMsg);
        return;
    }

    let loanValue = Number(prompt('How much do you want to loan?'));
    // Validate user input
    if(isNaN(loanValue) || loanValue === 0) alert('Please enter a valid number.');
    else if(loanValue > bankBalance * 2) alert('You cannot get a loan more than double of your bank balance.');
    // Transfer the money
    else {
        hasLoaned = true; // Set flag so that another loan won't be possible (without a purchase)
        loan = loanValue;
        bankBalance += loanValue;
        spnBankBalance.innerText = bankBalance;
        hideLoanElements(false);
        spnLoanBalance.innerText = loan;
    }
});

// Pay to Bank transfer listener
btnBank.addEventListener('click', () => {
    if(loan > 0) { // If there is a loan, deduct 10 % of the pay
        const deducted = payBalance * 0.10;
        payBalance -= deducted;
        loan -= deducted;
        if(loan < 0) { // Handle the case where the deducted money is greater than the loan
            bankBalance += -1 * loan; // Convert the negative loan amount to a positive number, add it to bank balance
            loan = 0; // Set the loan to zero
        }
        if(loan === 0) hideLoanElements(true);
        spnLoanBalance.innerText = loan;
    }
    bankBalance += payBalance;
    payBalance = 0;
    spnPayBalance.innerText = payBalance;
    spnBankBalance.innerText = bankBalance;
});

// Work event listener
btnWork.addEventListener('click', () => {
    payBalance += 100;
    spnPayBalance.innerText = payBalance;
});

// Repay listener
btnRepay.addEventListener('click', () => {
    loan -= payBalance;
    payBalance = 0;
    if(loan < 0) { // Handle the case where the pay money is greater than the loan
        bankBalance += -1 * loan; // Convert the negative loan amount to a positive number, add it to bank balance
        spnBankBalance.innerText = bankBalance;
        loan = 0;
    }
    spnPayBalance.innerText = payBalance;
    spnLoanBalance.innerText = loan;
    if(loan === 0) hideLoanElements(true);
});

// Computer select listener
sctComputer.addEventListener('change', function() { // No arrow function, as we have to bind "this" to context of the call
    const id = Number(this.value);
    const computer = findComputer(id);
    setComputer(computer);
});

// Buy computer listener
btnBuy.addEventListener('click', () => {
    const computerId = Number(sctComputer.value);
    const computer = findComputer(computerId);

    if(computer.price > bankBalance) {
        alert('Insufficient funds.');
        return;
    }

    // If possible, buy the computer
    bankBalance -= computer.price;
    spnBankBalance.innerText = bankBalance;
    hasLoaned = false; // Make it possible to get a loan again

    alert(`You are now a owner of the ${computer.name}`);
})

// Insert computers into selector
for(const computer of MOCK_COMPUTERS) {
    const optComputer = document.createElement('option');
    optComputer.value = computer.id;
    optComputer.innerText = computer.name;
    // If this is the first element to be added (no child nodes yet), set it to default selected
    if(!sctComputer.hasChildNodes()) {
        optComputer.selected = true;
        setComputer(computer); // Display information about this computer
    }
    sctComputer.appendChild(optComputer);
}

// Helper functions below
function hideLoanElements(val) {
    pLoanTag.hidden = val; // Show/hide the parent (the p tag) of the span, displaying "Loan: "
    btnRepay.hidden = val; // Show/hide the repay button
}

function setComputer(computer) {
    hedComputer.innerText = computer.name;
    pComputerDesc.innerText = computer.desc;
    spnComputerPrice.innerText = computer.price;
    imgComputer.src = computer.img;

    ulComputerFeatures.innerHTML = ''; // Remove current list items before setting new
    for (const feature of computer.features) {
        const liFeature = document.createElement('li');
        liFeature.innerText = feature;
        ulComputerFeatures.appendChild(liFeature);
    }
}

function findComputer(computerId) {
    return MOCK_COMPUTERS.find((computer) => computer.id === computerId);
}




