export function Computer(id, name, price, desc, features, img) {
    this.id = id;
    this.name = name;
    this.price = price;
    this.desc = desc;
    this.features = features;
    this.img = img;
}