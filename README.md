# Komputer Store

## About 
A web application that lets you work, transfer money, take up loans and buy laptops. The project does not use any frameworks
or libraries, and is created using plain HTML, CSS and JavaScript. 

## How to run
The project can be run by installing the npm package *http-server*, and then running `http-server . ` in the root folder. 

## Code structure
There are not that many separate files for this project. However, some of the CSS has been divided up into different files, and these are put into 
the folder `/css`. The main HTML and JS files can be found in the root folder, but there is a separate js file containing the Computer class,
that is put in the folder `/js`.
