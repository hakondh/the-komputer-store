//Computers
import {Computer} from "../js/computer.js";

export const MOCK_COMPUTERS = [
    new Computer(
        1,
        'Lenovo Laptop V15 IIL 82C500L1US',
        6144,
        'Achieve more in the office or on the go with the Lenovo V15, a durable and stylish 15.6" laptop that gets your everyday tasks done with efficiency while safeguarding your data.',
        ['Intel Core i5 10th Gen 1035G1 (1.00 GHz)', '8 GB Memory 256 GB PCIe SSD', 'Intel UHD Graphics', '1920 x 1080', 'Windows 10 Pro 64-bit'],
        'https://c1.neweggimages.com/ProductImageCompressAll1280/V009D200926BXO2T.jpg'
    ),
    new Computer(
        2,
        'ASUS ZenBook Duo UX481 14"',
        9729,
        'ASUS ZenBook Duo UX481 with ScreenPad™ Plus gives you on-the-go computing like you’ve never experienced before. A series of handy built-in apps help boost your productivity, giving you quick controls for intuitive interactions between the main display and ScreenPad™ Plus. ASUS also works closely with third-party developers to optimize ScreenPad™ Plus for the ultimate productivity. ScreenPad™ Plus is your gateway to endless possibilities.',
        ['ScreenPad Plus: 12.6 inch matte touchscreen, giving your endless way to optimize your multitasking experience by extending the screen or split windows and apps on both displays', '14 inch Full HD NanoEdge touchscreen glossy main display'],
        'https://c1.neweggimages.com/ProductImageCompressAll1280/34-235-507-12.jpg'
    ),
    new Computer(
        3,
        'Acer Laptop Aspire 5 A515-55-576H',
        5126,
        'Powerful and portable, the Aspire 5 laptop delivers on every aspect of everyday computing. Featuring the new 10th Gen Intel® Core™ i5 processor offering more possibilities than ever before—via performance, connectivity and entertainment. With advanced capabilities and a finely tuned design, the Aspire 5 supports a media-heavy lifestyle, making it ideal for anyone creating and sharing on the move. The narrow bezel design offers more screen real estate while the Full HD display and Acer Color Intelligence ensures crisp, true-to-life colors! It’s the perfect companion for your on-the-go lifestyle!',
        ['Intel Core i5 10th Gen 1035G1 (1.00 GHz)', '8 GB Memory 512 GB PCIe SSD', 'Intel UHD Graphics', '1920 x 1080', 'Windows 10 Home 64-bit'],
        'https://c1.neweggimages.com/ProductImageCompressAll1280/34-316-932-V83.jpg'
    ),
    new Computer(
        4,
        'HP 255 G7 Notebook, 15.6" HD Display',
        4659,
        'Power through your day with the HP 255 G7, a value-priced notebook loaded with decent performance, versatile connectivity, and essential collaboration tools in a durable chassis.',
        ['Customized HP 255 G7 Notebook', '15.6" HD Display', 'AMD A4-9125 2.3 - 2.6GHz', '8GB DDR4 RAM', 'Upgraded 512GB SSD', 'DVDRW, Card Reader', 'HDMI', 'LAN, Wi-Fi, Bluetooth', 'Windows 10 Pro'],
        'https://c1.neweggimages.com/ProductImageCompressAll1280/V0N6D201016FP0O7.jpg'
    )
]